# Compilation instructions

`cpp brownian.cpp -lpulse-simple`

This program requires the development headers and library of Pulseaudio, which should come with the Simple API. And you need to have Pulseaudio as your sound server. Converting this program to ALSA is an exercise for the reader..!
