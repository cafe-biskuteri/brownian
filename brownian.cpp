/* copyright

brownian - A basic brownian noise generator, under Pulseaudio
Written in 2023 by Usawashi <usawashi16@yahoo.co.jp>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

copyright */

#include <pulse/simple.h>
#include <iostream>
#include <cstdlib>
#include <climits>

int main(int nArgs, char* *args)
{
	using namespace std;
	srand(1997);
	
	int volume = 100;
	int channels = 2;
	int rate = 48000;
	float resolution = 1000.0;
	
	pa_sample_spec spec;
	spec.format = PA_SAMPLE_S16LE;
	spec.channels = channels;
	spec.rate = rate;
	
	pa_simple *audio = pa_simple_new(
		nullptr, /* Server */
		args[0], /* Name */
		PA_STREAM_PLAYBACK, /* Type */
		nullptr, /* Device */
		"Brownian noise", /* Description */
		&spec, /* Sample format */
		nullptr, /* Channel map */
		nullptr, /* Buffering attributes */
		nullptr /* Error code */
	);
	if (!audio)
	{
		cerr << "Pulseaudio error" << endl;
		return 1;
	}

	int MIN = SHRT_MIN;
	int MAX = SHRT_MAX;
	float STEP = MAX/resolution - MIN/resolution;
	int sample_count = channels * rate;
	int byte_count = sizeof(short) * sample_count;

	short buf[sample_count];
	float curr[channels], chg[channels];
	for (int co = 0; co < channels; ++co) curr[co] = 0;
	int err = 0;
	for (int o = 0; o < sample_count; o += channels)
	{
		/*
		for (int co = 0; co < channels; ++co)
		{
			chg[co] = STEP * (-1 + rand() % 3);
			if (curr[co] < (MIN - chg[co])) curr[co] = MIN;
			else if (curr[co] > (MAX - chg[co])) curr[co] = MAX;
			else curr[co] += chg[co];
			buf[o + co] = (short)curr[co] * volume/100;
		}
		/*/
		for (int co = 0; co < channels; ++co)
		{
			chg[0] = STEP * (-1 + rand() % 3);
			if (curr[0] < (MIN - chg[0])) curr[0] = MIN;
			else if (curr[0] > (MAX - chg[0])) curr[0] = MAX;
			else curr[0] += chg[0];
			buf[o + co] = (short)curr[0] * volume/100;
		}
		//*/
		
		if (o + channels >= sample_count)
		{
			pa_simple_write(audio, buf, byte_count, &err);
			if (err) {
				cerr << "Pulseaudio error" << endl;
				return 1;
			}
			o = -channels;
		}
	}
}
